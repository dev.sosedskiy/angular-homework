import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PersonsPageComponent } from './pages/persons-page/persons-page.component';

const routes: Routes = [
  { path: 'persons', component: PersonsPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
