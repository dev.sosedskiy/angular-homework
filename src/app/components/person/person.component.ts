import { Component, Input } from '@angular/core';
import { IPerson } from 'src/app/interface/Person';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.scss']
})
export class PersonComponent {

  @Input() person!: IPerson;  

  isVisible: boolean = true;

}
