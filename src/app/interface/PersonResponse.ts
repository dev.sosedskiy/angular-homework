import { IPerson } from "./Person";

export interface IPersonResponse{
    status: string,
    code: number,
    total: number,
    data: IPerson[]
}