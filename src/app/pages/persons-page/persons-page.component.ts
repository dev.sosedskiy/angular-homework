import { Component, OnInit } from '@angular/core';
import { IPerson } from 'src/app/interface/Person';
import { IPersonResponse } from 'src/app/interface/PersonResponse';
import { PersonService } from 'src/app/services/person.service';

@Component({
  selector: 'app-persons-page',
  templateUrl: './persons-page.component.html',
  styleUrls: ['./persons-page.component.scss']
})
export class PersonsPageComponent implements OnInit {

  persons: IPerson[] = []

  constructor(private _personService: PersonService) {}

  ngOnInit() : void{
    this._personService.getAllPersons()
    .subscribe( response => {
      this.persons = response.data
    } );
  }
  
}
