import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { IPersonResponse } from '../interface/PersonResponse';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PersonService {

  constructor( private _http: HttpClient ) { }

  getAllPersons():Observable<IPersonResponse>{
    return this._http.get<IPersonResponse>("https://fakerapi.it/api/v1/persons?_quantity=5&_gender=male&_birthday_start=2005-01-01")
  }
}
